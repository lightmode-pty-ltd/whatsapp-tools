# whatsappTools
Whatsapp Tools

## Contents
1. Intro
2. Examples

# 1. Intro

## How to install?

- composer require lightmode/whatsapp-tools
- php artisan vendor:publish
- php artisan migrate
- check config/whatsapp.php (just in case)
- add your API details to .env
- php artisan whatsapptools:setup
- check the examples below
- enjoy! 

## Samples

### .env sample config

WHATSAPP_API_ENDPOINT="https://wa.lightmode.co.za/api/v1/whatsapp"
WHATSAPP_API_KEY="AAAAAAAAA"
WHATSAPP_API_SECRET="ZZZZZZZZZZZZZZZZZZZ"


## Examples

### Sending a template
\App\WhatsappMessage::queueTextMessage('2779xxxxxxx89', 'hello_world');

### Sending a text message
\App\WhatsappMessage::queueTextMessage('2779xxxxxxx89', 'Hello text world!');
