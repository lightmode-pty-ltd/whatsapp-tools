<?php

namespace WhatsappTools;

use Illuminate\Support\ServiceProvider;

class WhatsappToolsServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot(\Illuminate\Routing\Router $router)
    {
        if (config('whatsapp.router.includeRoutes')) {
            $router->prefix(config('whatsapp.router.prefix'))
                ->namespace('WhatsappTools\Http\Controllers')
                ->middleware(config('whatsapp.router.middleware', []))
                ->group(__DIR__ . '/Http/api.php');
        }

        $argv = $this->app->request->server->get('argv');
        if (isset($argv[1]) and $argv[1] == 'vendor:publish') {
            $this->publishes([
                __DIR__ . '/../config/whatsapp.php' => config_path('whatsapp.php'),
            ], 'config');
            $this->publishes([
                __DIR__ . '/WhatsappMessage.php.stub' => app_path('WhatsappMessage.php'),
            ], 'model');

            $existing = glob(database_path('migrations/*_create_whatsapp_*'));
            if (empty($existing)) {
                $this->publishes([
                    __DIR__ . '/../database/migrations/create_whatsapp_messages.php.stub' => database_path('migrations/' . date('Y_m_d_His', time()) . '1_create_whatsapp_messages.php')
                ], 'migrations');
            }
        }
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(__DIR__ . '/../config/whatsapp.php', 'whatsapp');

        $this->app->bind('command.whatsapptools:setup', Commands\SetupCommand::class);

        $this->commands([
            'command.whatsapptools:setup',
        ]);

    }

}
