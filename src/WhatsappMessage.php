<?php

namespace WhatsappTools;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class WhatsappMessage extends Model
{
    const WEBHOOK_TYPE_WEBHOOK_TEST = 1;
    const WEBHOOK_TYPE_MESSAGE_QUEUED = 2;
    const WEBHOOK_TYPE_DELIVERY_REPORT = 3;
    const WEBHOOK_TYPE_MESSAGE_RECEIVED = 4;
    const WEBHOOK_TYPE_CONTACT_REPORT = 5;

    const MESSAGE_TYPE_TEMPLATE = 1;
    const MESSAGE_TYPE_TEXT = 2;
    const MESSAGE_TYPE_DOCUMENT = 3;
    const MESSAGE_TYPE_AUDIO = 4;
    const MESSAGE_TYPE_IMAGE = 5;
    const MESSAGE_TYPE_VIDEO = 6;
    const MESSAGE_TYPE_STICKER = 7;
    const MESSAGE_TYPE_LOCATION = 8;
    const MESSAGE_TYPE_CONTACT = 9;
    const MESSAGE_TYPE_LIST = 10;
    const MESSAGE_TYPE_INTERACTIVE_FLOW = 11;

    const MESSAGE_TYPE_AUTOMATED = 199;
    const MESSAGE_TYPE_SYSTEM = 200;

    public function getDetailsAttribute()
    {
        return json_decode($this->attributes['details'], true);
    }

    public function setDetailsAttribute($details)
    {
        $this->attributes['details'] = json_encode($details);
    }

    public static function queueTemplate(int $recipient, $templateName, $components = [], $sender = null)
    {
        if (!empty($components)) {
            $components = $components->toArray();
        }

        $message = new static();
        $message->type = self::MESSAGE_TYPE_TEMPLATE;
        $message->content = self::processTemplateContent($templateName, $components);
        $message->recipient = $recipient;
        $message->sender = $sender;
        $message->details = [
            'template' => $templateName,
            'components' => $components
        ];
        $message->created_at = now();
        $message->save();

        dispatch(new \WhatsappTools\Jobs\SendToGateway($message->id));

        return $message;
    }

    public static function processTemplateContent($templateName, $components)
    {
        return 'Template: ' . $templateName;
    }

    public static function queueTextMessage(int $recipient, $content, $sender = null)
    {
        $message = new self();
        $message->type = self::MESSAGE_TYPE_TEXT;
        $message->content = $content;
        $message->recipient = $recipient;
        $message->sender = $sender;
        $message->details = [];
        $message->created_at = now();
        $message->save();

        dispatch(new \WhatsappTools\Jobs\SendToGateway($message->id));

        return $message;
    }

    public static function queueInteractiveFlow(int $recipient, $flowId, $screenId, $components = [], $sender = null, $flowButtonName = 'Continue', $flowAction = 'navigate', $flowData = [])
    {
        $message = new self();
        $message->type = self::MESSAGE_TYPE_INTERACTIVE_FLOW;
        $message->content = '';
        $message->recipient = $recipient;
        $message->sender = $sender;
        $message->details = [
            'flow_id' => $flowId,
            'flow_button_name' => $flowButtonName,
            'flow_action' => $flowAction,
            'flow_data' => $flowData,
            'screen_id' => $screenId,
            'components' => $components,
        ];
        $message->created_at = now();
        $message->save();

        dispatch(new \WhatsappTools\Jobs\SendToGateway($message->id));

        return $message;
    }

    public static function setWebhook()
    {
        $requestData = [
            'url' => route(config('whatsapp.router.namedPrefix') . '.' . config('whatsapp.router.webhookEndpoint')),
        ];

        $session = curl_init(config('whatsapp.api.endpoint') . '/webhook');
        curl_setopt($session, CURLOPT_POST, true);
        curl_setopt($session, CURLOPT_POSTFIELDS, $requestData);

        curl_setopt($session, CURLOPT_HTTPHEADER, [
            'x-api-key: ' . config('whatsapp.api.key'),
            'x-api-secret: ' . config('whatsapp.api.secret')
        ]);

        curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($session, CURLOPT_FOLLOWLOCATION, true);
        $response = curl_exec($session);
        curl_close($session);

        return json_decode($response, true);
    }

    public function sendToGateway()
    {
        if (!$this->queued) {
            $sender = $this->sender;

            if (is_null($sender)) {
                $sender = config('official-whatsapp.api.number');
            }

            switch ($this->type) {
                case self::MESSAGE_TYPE_TEMPLATE:
                    $requestData = [
                        'recipient' => $this->recipient,
                        'template' => $this->details['template'],
                        'components' => json_encode($this->details['components']),
                        'sender' => $sender,
                    ];
                    $url = config('whatsapp.api.endpoint') . '/sendTemplate';
                    break;

                case self::MESSAGE_TYPE_TEXT:
                    $requestData = [
                        'recipient' => $this->recipient,
                        'content' => $this->content,
                        'sender' => $sender,
                    ];
                    $url = config('whatsapp.api.endpoint') . '/sendMessage';
                    break;

                case self::MESSAGE_TYPE_AUTOMATED:
                    $requestData = [
                        'recipient' => $this->recipient,
                        'content' => $this->content,
                        'sender' => $sender,
                    ];
                    $url = config('whatsapp.api.endpoint') . '/sendAutomated';
                    break;

                case self::MESSAGE_TYPE_INTERACTIVE_FLOW:
                    $requestData = [
                        'recipient' => $this->recipient,
                        'sender' => $sender,
                        'components' => json_encode($this->details['components']),
                        'flow_id' => $this->details['flow_id'],
                        'screen_id' => $this->details['screen_id'],
                        'flow_button_name' => $this->details['flow_button_name'],
                        'flow_action' => $this->details['flow_action'],
                        'flow_data' => json_encode($this->details['flow_data']),
                    ];
                    $url = config('whatsapp.api.endpoint') . '/sendInteractiveFlow';
                    break;
            }

            $session = curl_init($url);

            curl_setopt($session, CURLOPT_POST, true);
            curl_setopt($session, CURLOPT_POSTFIELDS, $requestData);

            curl_setopt($session, CURLOPT_HTTPHEADER, [
                'x-api-key: ' . config('whatsapp.api.key'),
                'x-api-secret: ' . config('whatsapp.api.secret')
            ]);

            curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($session, CURLOPT_FOLLOWLOCATION, true);
            $response = curl_exec($session);

            curl_close($session);

            ray('Gateway response:' . $response);

            $response = json_decode($response, true);

            if (!empty($response)) {
                if (isset($response['id'])) {
                    $this->queued = true;
                    $this->gateway_id = $response['id'];
                    $this->save();
                }

                return true;
            }

            return false;
        }
    }

    public static function checkWebhookSignature()
    {
        $payload = request()->get('payload');
        $signature = request()->get('signature');
        $string = config('whatsapp.api.key') . '---' . trim(json_encode($payload['hash']), '{}') . '---' . config('whatsapp.api.secret');
        $calculatedSignature = md5($string);
        if ($signature != $calculatedSignature) {
            throw new \Exception('Wrong signature');
        }
    }

    public static function updateFromPayload($payload)
    {
        $message = static::query()
            ->where('gateway_id', $payload['message']['id'])
            ->first();

        if (is_null($message)) {
            return false;
        }

        $message->sender = $payload['message']['sender'];
        $message->sent = $payload['message']['sent'];
        $message->failed = $payload['message']['failed'];
        $message->delivered = $payload['message']['delivered'];
        $message->read = $payload['message']['read'];
        $details = $message->details;
        $details['sender'] = $payload['message']['sender'];
        $message->reply_message_id = $payload['message']['reply_message_id'];
        $message->reaction = $payload['message']['reaction'];
        $message->has_media = $payload['message']['has_media'];
        $message->media_url = $payload['message']['media_url'];
        $message->details = $details;
        $message->queued_at = $payload['message']['queued_at'];
        $message->sent_at = $payload['message']['sent_at'];
        $message->delivered_at = $payload['message']['delivered_at'];
        $message->read_at = $payload['message']['read_at'];

        $message->save();

        return $message;
    }

    public static function createFromPayload($payload)
    {
        $message = new static();
        $message->gateway_id = $payload['message']['id'];
        $message->type = $payload['message']['type'];
        $message->recipient = $payload['message']['recipient'];
        $message->sender = $payload['message']['sender'];
        $message->content = $payload['message']['content'];
        $message->queued = $payload['message']['queued'];
        $message->sent = $payload['message']['sent'];
        $message->failed = $payload['message']['failed'];
        $message->delivered = $payload['message']['delivered'];
        $message->read = $payload['message']['read'];
        $message->has_media = $payload['message']['has_media'];
        $message->media_url = $payload['message']['media_url'];
        $message->details = [];
        $message->created_at = now();
        $message->sent_at = $payload['message']['sent_at'];
        $message->delivered_at = $payload['message']['delivered_at'];
        $message->read_at = $payload['message']['read_at'];
        $message->save();

        return $message;
    }

    public static function processRequest()
    {
        try {
            static::checkWebhookSignature();
        } catch (\Exception $e) {
            return response()->json([
                'error' => [
                    'message' => 'Invalid signature - unauthorized',
                    'status_code' => 403
                ]
            ], 403);
        }

        $type = request()->get('type');
        $payload = request()->get('payload');

        switch ($type) {
            case self::WEBHOOK_TYPE_WEBHOOK_TEST:
                return response()->json(['tested' => true]);
                break;

            case self::WEBHOOK_TYPE_MESSAGE_QUEUED:
                return response()->json(['success' => static::updateFromPayload($payload)]);
                break;

            case self::WEBHOOK_TYPE_DELIVERY_REPORT:
                $payload = static::updateFromPayload($payload);

                if (method_exists(static::class, 'messageReport')) {
                    static::messageReport($payload);
                }

                return response()->json(['success' => (bool)$payload]);
                break;

            case self::WEBHOOK_TYPE_MESSAGE_RECEIVED:
                $payload = static::createFromPayload($payload);

                if (method_exists(static::class, 'messageReceived')) {
                    static::messageReceived($payload);
                }

                return response()->json(['success' => (bool)$payload]);
                break;

            case self::WEBHOOK_TYPE_CONTACT_REPORT:
                if (method_exists(static::class, 'contactReport')) {
                    static::contactReport($payload);
                }

                return response()->json(['success' => (bool)$payload]);
                break;

            default:
                return response()->json([
                    'error' => [
                        'message' => 'Invalid type',
                        'status_code' => 422
                    ]
                ], 422);
                break;
        }
    }

}
