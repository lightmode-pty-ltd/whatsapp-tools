<?php

namespace WhatsappTools\Http\Controllers;

class WhatsappController extends \App\Http\Controllers\Controller
{
    public function webhook()
    {
        $messageClass = config('whatsapp.message_model');
        $messageInstance = new $messageClass();

        return $messageInstance::processRequest();
    }
}
