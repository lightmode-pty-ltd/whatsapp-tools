<?php

$router->post(config('whatsapp.router.webhookEndpoint'), ['uses' => 'WhatsappController@webhook', 'as' => config('whatsapp.router.namedPrefix') . '.webhook']);

