<?php

namespace WhatsappTools;

use Carbon\Carbon;

class WhatsappTemplate
{
    const TEMPLATE_LANGUAGE_EN_US = 1;

    const TEMPLATE_STATUS_PENDING = 1;
    const TEMPLATE_STATUS_REJECTED = 2;
    const TEMPLATE_STATUS_APPROVED = 3;

    const TEMPLATE_TYPE_MARKETING = 1;
    const TEMPLATE_TYPE_UTILITY = 2;
    const TEMPLATE_TYPE_AUTHENTICATION = 3;

    public static function getTemplates()
    {
        $requestData = [];

        $url = config('whatsapp.api.endpoint') . '/getTemplates';

        $session = curl_init($url);
        curl_setopt($session, CURLOPT_POST, true);
        curl_setopt($session, CURLOPT_POSTFIELDS, $requestData);

        curl_setopt($session, CURLOPT_HTTPHEADER, [
            'x-api-key: ' . config('whatsapp.api.key'),
            'x-api-secret: ' . config('whatsapp.api.secret')
        ]);

        curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($session, CURLOPT_FOLLOWLOCATION, true);
        $response = curl_exec($session);
        curl_close($session);

        return json_decode($response, true);
    }
}
