<?php

return [
    'router' => [
        'includeRoutes' => true,
        'prefix' => 'whatsapptools',
        'namedPrefix' => 'whatsapp-tools',
        'webhookEndpoint' => 'webhook',
        'middleware' => [],
    ],

    'api' => [
        'endpoint' => env('WHATSAPP_API_ENDPOINT', 'https://wa.lightmode.co.za/api/v1/whatsapp'),
        'key' => env('WHATSAPP_API_KEY'),
        'secret' => env('WHATSAPP_API_SECRET'),
        'webhook' => env('WHATSAPP_API_WEBHOOK'),

        'number' => env('WHATSAPP_API_DEFAULT_NUMBER', null)
    ],

    'message_model' => \App\WhatsappMessage::class,
];

